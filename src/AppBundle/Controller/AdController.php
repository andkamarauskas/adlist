<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Ad;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AdController extends Controller
{
    /**
     * @Route("/", name="ads")
     */
    public function indexAction()
    {
        $ads = $this->getDoctrine()
        ->getRepository('AppBundle:Ad')
        ->findAll();
        return $this->render('ads/index.html.twig', array(
            'ads' =>$ads
        ));
    }

    /**
     * @Route("/ad/user", name="user_ads")
     */
    public function UserAdsAction()
    {
        $user = $this->getUser();
        $ads = $user->getAds();
        if(count($ads)>0)
        {
            return $this->render('user/ads.html.twig', array('ads' =>$ads));   
        }
        else
        {
           $this->addFlash(
                'warning',
                'You do not have ads already'
            ); 
            return $this->redirectToRoute('ads');   
        }
    }

    /**
     * @Route("/ad/create", name="create_ad")
     */
    public function createAction(Request $request)
    {
        $ad = new Ad;

        $form = $this->createFormBuilder($ad)
        ->add('title', TextType::class, array('attr' => array('class' => 'form-control')))
        ->add('description', TextareaType::class, array('attr' => array('class' => 'form-control')))
        ->add('save', SubmitType::class, array('label' => 'Create' , 'attr' => array('class' => 'btn btn-block btn-primary', 'style' =>'margin-top:10px')))
        ->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $title = $form['title']->getData();
            $description = $form['description']->getData();

            $now = new\Datetime('now');
            $user = $this->getUser();
            $ad->setTitle($title);
            $ad->setDescription($description);
            $ad->setCreatedAt($now);
            $ad->setUser($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($ad);
            $em->flush();

            $this->addFlash(
                'ok',
                'New Ad Added'
            );
            return $this->redirectToRoute('ads');
        }
        return $this->render('ads/create.html.twig', array(
            'form' => $form->createView()
        ));
    }

}
